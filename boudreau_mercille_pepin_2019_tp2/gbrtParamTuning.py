import csv
import subprocess
import math
import os 
import numpy as np
from sklearn import svm
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import GradientBoostingClassifier

# Code inspired from https://chrisalbon.com/machine_learning/model_evaluation/cross_validation_parameter_tuning_grid_search/?fbclid=IwAR0J1SSe4exEsP79ciRqGYYSLcjhDiAAPy02cUGp5O4nhG4FmuaEG3qlY0c

if(os.path.isfile('./ds1Train.csv') == False):
    subprocess.call('python3 fetchDataset.py')

resultsFile = open("gbrt_param_tuning.csv", 'w+') 
resultsFile.write("learning_rate, n_estimators, max_depth, min_samples_split, min_samples_leaf, best_score\n")

data = []
classes = []
with open("ds1Train.csv") as trainingFile:
    reader = csv.reader(trainingFile)
    for row in reader:
        classes.append(row.pop())
        data.append(row)

parametersSets = [{'learning_rate': [1, 0.5], 'n_estimators': [1, 10], 'max_depth': np.linspace(1, 6, 2, dtype=int),
                'min_samples_split': np.linspace(2, 10, 2, dtype=int), 'min_samples_leaf': np.linspace(2, 10, 2, dtype=int)}]

model = GridSearchCV(estimator=GradientBoostingClassifier(), param_grid=parametersSets, n_jobs=-1)
model.fit(data, classes)
currentBestScore = model.best_score_
learning_rate_best_value = model.best_estimator_.learning_rate
n_estimators_best_value = model.best_estimator_.n_estimators
max_depth_best_value = model.best_estimator_.max_depth
min_samples_split_best_value = model.best_estimator_.min_samples_split
min_samples_leaf_best_value = model.best_estimator_.min_samples_leaf
print(str(learning_rate_best_value) + ", " + str(n_estimators_best_value) + ", " + str(max_depth_best_value) + ", " + str(min_samples_split_best_value) + ", " + str(min_samples_leaf_best_value) + ", " + str(currentBestScore))
resultsFile.write(str(learning_rate_best_value) + ", " + str(n_estimators_best_value) + ", " + str(max_depth_best_value) + ", " + str(min_samples_split_best_value) + ", " + str(min_samples_leaf_best_value) + ", " + str(currentBestScore) + "\n")
learning_rate_step = learning_rate_best_value / 2.0
n_estimators_step = n_estimators_best_value / 2.0
max_depth_step = max_depth_best_value / 2.0
min_samples_split_step = min_samples_split_best_value / 2.0
min_samples_leaf_step = min_samples_leaf_best_value / 2.0

scoreIsImproving = True
threshold = 0.001
nSamples = 3
learning_rate_absolute_min_value = 0.0001
n_estimators_absolute_min_value = 1
max_depth_absolute_min_value = 1
min_samples_split_absolute_min_value = 2
min_samples_leaf_absolute_min_value = 2
counter = 0

while (scoreIsImproving or counter < 2) :
    
    # learning_rate samples
    learning_rate_max_value = math.ceil(learning_rate_best_value + learning_rate_step)
    learning_rate_min_value = math.floor(learning_rate_best_value - learning_rate_step)
    if learning_rate_min_value < learning_rate_absolute_min_value:
        learning_rate_min_value = learning_rate_absolute_min_value
        learning_rate_sample = np.linspace(learning_rate_min_value, learning_rate_max_value, 2)
    else:
        learning_rate_sample = np.linspace(learning_rate_min_value, learning_rate_max_value, nSamples)

    # max_depth samples
    max_depth_max_value = math.ceil(max_depth_best_value + max_depth_step)
    max_depth_min_value = math.floor(max_depth_best_value - max_depth_step)
    if max_depth_min_value < max_depth_absolute_min_value:
        max_depth_min_value = max_depth_absolute_min_value
        max_depth_sample = np.linspace(max_depth_min_value, max_depth_max_value, 2, dtype=int)
    else:
        max_depth_sample = np.linspace(max_depth_min_value, max_depth_max_value, nSamples, dtype=int)
    
    # n_estimators samples
    n_estimators_max_value = math.ceil(n_estimators_best_value + n_estimators_step)
    n_estimators_min_value = math.floor(n_estimators_best_value - n_estimators_step)
    if n_estimators_min_value < n_estimators_absolute_min_value:
        n_estimators_min_value = n_estimators_absolute_min_value
        n_estimators_sample = np.linspace(n_estimators_min_value, n_estimators_max_value, 2, dtype=int)   
    else: 
        n_estimators_sample = np.linspace(n_estimators_min_value, n_estimators_max_value, nSamples, dtype=int)

     # min_samples_split samples
    min_samples_split_max_value = math.ceil(min_samples_split_best_value + min_samples_split_step)
    min_samples_split_min_value = math.floor(min_samples_split_best_value - min_samples_split_step)
    if min_samples_split_min_value < min_samples_split_absolute_min_value:
        min_samples_split_min_value = min_samples_split_absolute_min_value
        min_samples_split_sample = np.linspace(min_samples_split_min_value, min_samples_split_max_value, 2, dtype=int)   
    else: 
        min_samples_split_sample = np.linspace(min_samples_split_min_value, min_samples_split_max_value, nSamples, dtype=int)
    
    # min_samples_leaf samples
    min_samples_leaf_max_value = math.ceil(min_samples_leaf_best_value + min_samples_leaf_step)
    min_samples_leaf_min_value = math.floor(min_samples_leaf_best_value - min_samples_leaf_step)
    if min_samples_leaf_min_value < min_samples_leaf_absolute_min_value:
        min_samples_leaf_min_value = min_samples_leaf_absolute_min_value
        min_samples_leaf_sample = np.linspace(min_samples_leaf_min_value, min_samples_leaf_max_value, 2, dtype=int)   
    else: 
        min_samples_leaf_sample = np.linspace(min_samples_leaf_min_value, min_samples_leaf_max_value, nSamples, dtype=int)
    

    parametersSets = [{'learning_rate': learning_rate_sample, 'max_depth': max_depth_sample, 'n_estimators': n_estimators_sample, 'min_samples_split': min_samples_split_sample, 'min_samples_leaf': min_samples_leaf_sample}]
    model = GridSearchCV(estimator=GradientBoostingClassifier(), param_grid=parametersSets, n_jobs=-1)
    model.fit(data, classes)
    score = model.best_score_
    computedSameParameters = (model.best_estimator_.learning_rate == learning_rate_best_value) and (model.best_estimator_.max_depth == max_depth_best_value) and (model.best_estimator_.n_estimators == n_estimators_best_value) and (model.best_estimator_.min_samples_split == min_samples_split_best_value) and (model.best_estimator_.min_samples_leaf == min_samples_leaf_best_value)
    scoreIsImproving = ((score - currentBestScore) > threshold) or (computedSameParameters == True)
    if(score > currentBestScore):
        currentBestScore = score

    # Step tunning
    learning_rate_step = model.best_estimator_.learning_rate / 2.0
    if(model.best_estimator_.learning_rate == learning_rate_best_value):
        learning_rate_step = learning_rate_step / 2.0
    max_depth_step = model.best_estimator_.max_depth / 2.0
    if(model.best_estimator_.max_depth == max_depth_best_value):
        max_depth_step = max_depth_step / 2.0
    n_estimators_step = model.best_estimator_.n_estimators / 2.0
    if(model.best_estimator_.n_estimators == n_estimators_best_value):
        n_estimators_step = n_estimators_step / 2.0
    min_samples_split_step = model.best_estimator_.min_samples_split / 2.0
    if(model.best_estimator_.min_samples_split == min_samples_split_best_value):
        min_samples_split_step = min_samples_split_step / 2.0
    min_samples_leaf_step = model.best_estimator_.min_samples_leaf / 2.0
    if(model.best_estimator_.min_samples_leaf == min_samples_leaf_best_value):
        min_samples_leaf_step = min_samples_leaf_step / 2.0
    

    learning_rate_best_value = model.best_estimator_.learning_rate
    n_estimators_best_value = model.best_estimator_.n_estimators
    min_samples_split_best_value = model.best_estimator_.min_samples_split
    min_samples_leaf_best_value = model.best_estimator_.min_samples_leaf
    
    resultsFile.write(str(learning_rate_best_value) + ", " + str(n_estimators_best_value) + ", " + str(max_depth_best_value) + ", " + str(min_samples_split_best_value) + ", " + str(min_samples_leaf_best_value) + ", " + str(currentBestScore) + "\n")
    print(str(learning_rate_best_value) + ", " + str(n_estimators_best_value) + ", " + str(max_depth_best_value) + ", " + str(min_samples_split_best_value) + ", " + str(min_samples_leaf_best_value) + ", " + str(currentBestScore))
    counter = counter + 1
    computedSameParameters = False

resultsFile.close()
