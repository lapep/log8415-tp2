import argparse
import csv
import os 
import subprocess
import pickle
from sklearn import svm

parser = argparse.ArgumentParser()
parser.add_argument('gamma', help="gamma parameter value", type=float)
parser.add_argument('C', help="C parameter value", type=float)
parser.add_argument('degree', help="degree parameter value", type=float)
args = parser.parse_args()
 
if(os.path.isfile('./ds1Train.csv') == False or os.path.isfile('./ds1Val.csv') == False):
    subprocess.call(["python3", "fetchDataset.py"])

data = []
classes = []
with open("ds1Train.csv") as trainingFile:
    reader = csv.reader(trainingFile)
    for row in reader:
        classes.append(row.pop())
        data.append(row)

# Train  model  with  all  
datamodel = svm.SVC(kernel='poly', gamma=args.gamma, degree=args.degree, C=args.C).fit(data, classes)
trainingFile.close()

# Validate sanity of result
data = []
classes = []
with open("ds1Val.csv") as valFile:
    reader = csv.reader(valFile)
    for row in reader:
        classes.append(row.pop())
        data.append(row)

valFile.close()
score = datamodel.score(data, classes)
print("Score with validation data set is: " + str(score))

# Save
modelf = open('model_svm.pckl', 'wb')
pickle.dump(datamodel , modelf)
modelf.close ()
subprocess.call(["python3", "uploadFileToBucket.py", "model_svm.pckl"])
