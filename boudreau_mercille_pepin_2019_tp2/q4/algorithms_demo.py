#!/usr/bin/python3
from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.types import DoubleType, StringType, StructField, StructType, IntegerType
from pyspark.ml.classification import DecisionTreeClassifier
from pyspark.ml.feature import VectorAssembler, StringIndexer
from pyspark.ml import Pipeline

if __name__ == "__main__":
	spark = SparkSession.builder.appName("dawdawdawdwa").getOrCreate()

	# Define data structure
	schema = StructType([
		StructField("age", IntegerType()),
		StructField("workclass", StringType()),
		StructField("fnlwgt", IntegerType()),
		StructField("education", StringType()),
		StructField("education-num", IntegerType()),
		StructField("marital-status", StringType()),
		StructField("occupation", StringType()),
		StructField("relationship", StringType()),
		StructField("race", StringType()),
		StructField("sex", StringType()),
		StructField("capital-gain", IntegerType()),
		StructField("capital-loss", IntegerType()),
		StructField("hours-per-week", IntegerType()),
		StructField("revenue", StringType())])

	# Read raw data
	df = spark.read.csv("adults_data.csv", header=True, schema=schema,mode="DROPMALFORMED")

	# Split data in 2 sets: training set and prediction set
	splits = df.randomSplit([0.9, 0.1])
	trainingData = splits[0]
	predictionData = splits[1]

	# Convert string columns to int (using indexes)
	workclassIndexer = StringIndexer().setInputCol("workclass").setOutputCol("workclassIndex")
	educationIndexer = StringIndexer().setInputCol("education").setOutputCol("educationIndex")
	maritalStatusIndexer = StringIndexer().setInputCol("marital-status").setOutputCol("maritalStatusIndex")
	occupationIndexer = StringIndexer().setInputCol("occupation").setOutputCol("occupationIndex")
	relationshipIndexer = StringIndexer().setInputCol("relationship").setOutputCol("relationshipIndex")
	raceIndexer = StringIndexer().setInputCol("race").setOutputCol("raceIndex")
	sexIndexer = StringIndexer().setInputCol("sex").setOutputCol("sexIndex")
	revenueIndexer = StringIndexer().setInputCol("revenue").setOutputCol("revenueIndex")

	# Regroup all features in a single vector
	assembler = VectorAssembler().setInputCols([
		"age",
		"workclassIndex",
		"fnlwgt",
		"educationIndex",
		"education-num",
		"maritalStatusIndex",
		"occupationIndex",
		"relationshipIndex",
		"raceIndex",
		"sexIndex",
		"capital-gain",
		"capital-loss",
		"hours-per-week"]).setOutputCol("features")
	
	dt = DecisionTreeClassifier().setLabelCol("revenueIndex").setPredictionCol("Predicted revenue").setFeaturesCol("features")
	
	# Use pipeline to format data properly
	stages = [workclassIndexer, educationIndexer, maritalStatusIndexer, occupationIndexer, relationshipIndexer, raceIndexer, sexIndexer, revenueIndexer, assembler, dt ]
	pipeline = Pipeline(stages = stages)
	
	# Train the Classifier using trainingData
	model = pipeline.fit(trainingData)

	# Predict predictionData's revenues, based on the features and the training
	model.transform(predictionData).show()

	spark.stop()
