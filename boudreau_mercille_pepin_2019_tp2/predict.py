import json
import boto3
import botocore
import pickle
import os
import ctypes
import numpy

for d, _, files in os.walk('lib'):
    for f in files:
        if f.endswith('.a'):
            continue
        ctypes.cdll.LoadLibrary(os.path.join(d, f))

#import sklearn

def lambda_handler(event, context):

    
    BUCKET_NAME = 'log8430tp2' 
    KEY = 'model_svm.pckl'
    s3 = boto3.resource('s3')
    
    # Fetch model
    
    try:
        s3.Bucket(BUCKET_NAME).download_file(KEY, '/tmp/model_svm.pckl')
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("The object does not exist.")
        else:
            raise
    
    # Make predictions
    
    f = open('/tmp/model_svm.pckl' , 'rb')
    model = pickle.load(f)
    f.close ()
    
    input = event['values']
    predictions = model.predict(input).tolist() 
    return {
        'statusCode': 200,
        'body': predictions
    }
