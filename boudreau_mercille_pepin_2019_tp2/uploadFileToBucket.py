import boto3
import botocore
import sys
BUCKET_NAME = 'log8430tp2' 
KEY = sys.argv[1]
s3 = boto3.client('s3')

try:
    s3.upload_file(KEY, BUCKET_NAME, KEY)
except botocore.exceptions.ClientError as e:
    if e.response['Error']['Code'] == "404":
        print("The object does not exist.")
    else:
        raise
