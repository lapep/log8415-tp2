\documentclass[11pt]{article}
\usepackage{amssymb}
\usepackage[english]{babel}
\usepackage{fullpage}
\usepackage{graphicx,multirow}
\usepackage{caption}
\captionsetup{font=bf,belowskip=8pt}
\usepackage{hyperref}
\usepackage{enumitem}
\usepackage{placeins}

\begin{document}
\begin{titlepage} 
	\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}
	\center
	\textsc{\LARGE Polytechnique Montréal}\\[1.5cm]
	\textsc{\Large LOG8415 : Lab 2}\\[0.5cm]
	\textsc{\large Advanced Concepts in Cloud Computing}\\[0.5cm]
	\HRule\\[0.4cm]
	{\huge\bfseries Spark and AWS Lambda}\\[0.4cm]
	\HRule\\[1.5cm]
	{\large\textit{Authors}}\\
	Louis \textsc{Boudreau} (1791639)\\
	Laurent \textsc{Pepin} (1739608)\\
	Tristan \textsc{Mercille-Brunelle} (1740701)\\
	\vfill\vfill\vfill {\large\today} \vfill\vfill
	\includegraphics{poly_logo.png}\\[1cm]
	\vfill
\end{titlepage}

\tableofcontents \pagebreak

\section{Introduction} \label{sec:introduction}
	\paragraph{} This work shows an introduction to the Spark technology
	and/with machine learning algorithms using Amazon Web Services EC2, S3 and
	Lambda services. The goals are to experiment with Spark on a EC2 machine,
	learn to upload and download objects to and from a S3 bucket, execute a
	server-less function and learn more about machine learning classification
	algorithms. The following sections present a simple Spark program to count
	words in a file, answers to theoretical questions and a complete overview of
	predictions generation using Spark and machine learning algorithms.

\section{Experiments with WordCount} \label{sec:wordcount}
	\subsection{Setup} \paragraph{} For our first experience with Apache Spark,
	we used a AWS instance with 32G RAM and 8 vcpu (t2.2xlarge) running the
	latest version of ubuntu (18.04). To run the WordCount program, we installed
	Spark, Python version 3.7.2 and the boto3 library, which facilitates AWS
	actions through python. We also set up a credentials file in order to allow
	connection to our AWS account and perform file uploads and downloads to S3,
	and much more.

	\subsection{Code} 
		\paragraph{}The subject of the WordCount program is a file named
		219-0.txt, already in our S3 bucket. In order to run the program, we
		first had to download the file from S3. We use the boto3 library command
		download\_file to download the text file on the virtual machine. The
		script that executes this command is named \emph{word\_count\_job.py}. After
		successfully downloading the file, the python script \emph{wordcount.py} is
		called.

		\paragraph{}The script \emph{wordcount.py} contains the logic to count the words
		in the file using Spark. We applied three Spark transformations on the
		dataset (loaded text file). First, flatMap was used to split all the
		lines into words using the space character as a separator. Then, map was
		used to transform these words into tuples, where the word is the key and
		the value is 1. Finally, reduceByKey was applied to the dataset
		containing the tuples. All the tuples with the same key (same word) were
		combined and their values added. Results are then saved in files using
		the Spark action \emph{saveAsTextFile}. \cite{1}
	\subsection{Results}
		\paragraph{}Spark action \emph{saveAsTextFile} created two files named
		\emph{part-00000} and \emph{part-00001}. This means that two reducers were used to
		solve the WordCount problem. Each of these files contains tuples (one
		per line) where the key is a word in the text file \emph{219-0.txt} and the
		value is the number of occurrences in the text file. Because of the use
		of \emph{reduceByKey} no tuples with the same key appear in the two
		files/partitions. Here is an example of the results obtained, the first
		5 lines of \emph{part-00000} file.\\ 
		('The', 211)\\
		('Project', 78)\\ 
		('EBook', 2)\\
		('of', 1460)\\
		('Darkness,', 3)

\section{Questions}
	\subsection{Question 1}
		\paragraph{}Spark has many transformations and actions. In the source
		code submitted with this work, examples of transformations and actions
		are demonstrated in the file named \emph{sparkCommands.py}. In this file,
		transformations and actions are commented to provide their description.
		Here is the list of transformations and actions in Spark according to
		their official documentation. \cite{1}\\
		\textbf{Transformations:} map, filter, flatMap, mapPartitions,
		mapPartitionsWithIndex, sample, union, intersection, disting,
		groupByKey, reduceByKey, aggregateByKey, sortByKey, join, cogroup,
		cartesian, pipe, coalesce, repartition,
		repartitionAndSortWithinPartitions\\
		\textbf{Actions:} reduce, collect, count, first, take, takeSample,
		takeOrdered, saveAsTextFile, saveAsSequenceFile, saveAsObjectFile,
		countByKey, foreach
	\subsection{Question 2}
		\paragraph{}In order to clearly define the relation between Apache Spark
		and MapReduce, an overview of the MapReduce is needed. The following
		section will therefore present the main concepts of this well known
		paradigm. 
		
		\paragraph{}\emph{MapReduce} concept was first presented to the
		literature in 2004 by two former Google researchers. This programming
		model tries to hide the complexity of distributing computations on a
		cloud cluster and keep the focus on the simple computations needed to
		digest the data. In fact, this model is composed of only two operations
		which are respectively Map and Reduce. Map is the function responsible
		for taking a certain key/value pair and transforming it to an
		intermediate key/value pair. On the other hand, Reduce function takes
		the set of intermediate values associated to a particular key and
		combines them to create the result value for that key. It is also worth
		noting that the Master/Slave design pattern is used to orchestrate jobs
		across different node on the cluster.  The complete lifecycle of a
		MapReduce job is therefore the following. It all starts by splitting the
		dataset into multiple chunks. Once this is done, the master node starts
		distributing Map jobs for a particular chunk to some idle node. The
		corresponding node therefore reads it and parses it into some key/value
		pairs. The Map function is then computed on all of the previously
		computed pairs and the intermediate results are stored in memory. These
		results are periodically written back to the local disk and these
		locations are forwarded back to the master. Once all the mapping jobs
		are done, the intermediate key/value pairs are sorted and grouped by
		keys. All the keys and their respective set of values are fetched and
		passed to a Reducer function by an available node. The result of the
		Reduce function is written to the result file of the given data split.
		The first framework that emerged from this paradigm is the famous Hadoop
		Framework. 
		
		\paragraph{}Spark on the other hand was presented to the peers in 2010
		by Berkeley University PhD research students. The scope of this
		framework was to propose an efficient big data framework for
		applications where MapReduce was lacking performance while still
		preserving scalability and fault tolerance. The types of applications
		for which MapReduce isn’t performing are the ones which iteratively
		reuse the same set of data across multiple parallel computations.
		MapReduce isn’t efficient in this context since the only way to share
		data between two MapReduce jobs is from reading and writing to the
		disk. Spark solves this problem by enabling in memory persistence,
		therefore eliminating the heavy cost associated with I/O operations.
		This persistence is made possible by a data structure named Resilient
		Distributed Dataset(RDD).  RDD’s are composed lists of objects each
		containing a description of how they were computed and a pointer to
		their parent. Therefore, if a node fails, any needed piece of data can
		be rebuilt from scratch by traversing the RDD inversingly.

		\paragraph{}We can therefore conclude that the relation between
		Spark and MapReduce is the following. Spark is a framework that
		implements the Map Reduce design pattern but differs from the Hadoop
		implementation. RDD’s and memory persistence make this framework
		efficient for iterative computations. \cite{4}\cite{5}\cite{6}

	\subsection{Question 3}
		\paragraph{}DataBricks is a company whose founders have developed the
		Spark framework. In 2014, Databricks started a certification called
		“Certified Spark Distribution” which aims at indicating that a vendor with a
		commercial Spark license is compatible with the open source Spark
		distribution. The following companies are some of the ones with possession
		of a commercial Spark license \cite{15} : 
		\begin{itemize}
			\item IBM
			\item Pivotal
			\item HortonsWorks
			\item Oracle
			\item DataStax
			\item Databricks
			\item MapR
			\item Cloudera
		\end{itemize}

		\paragraph{}Going for a commercial big data software as a service that
		is using Apache Spark under the hood has many advantages:
		\begin{itemize}
			\item Automatically optimizes parameter tuning
			\item Provides rich UI interfaces to simplify data understanding
			\item Provides other frameworks such as Hadoop and Apache Kafka to provide a more complete big data solution
			\item Simplifies deployment of big data applications
		\end{itemize}

		\paragraph{}These kind of softwares also have many drawbacks:
		\begin{itemize}
			\item Lose control of details
			\item Costly
			\item Can have some failures at crucial times
			\item Don’t have full control over data storing
		\end{itemize}

	\subsection{Question 4}
		\paragraph{}Here’s a list of all machine learning algorithms supported
		by MLLib, in the format:\\AlgorithmName - \textbf{MLLibsAgorithmMethod}

		\begin{itemize}
			\item Classification 
			\begin{itemize}
				\item Multinomial logistic regression - \textbf{LogisticRegression(family=”multinomial”)}
				\item Binomial logistic regression - \textbf{LogisticRegression(...)}
				\item Decision tree classifier - \textbf{DecisionTreeClassifier(...)}
				\item Random forest classifier - \textbf{RandomForestClassifier(...)}
				\item Gradient-boosted tree classifier - \textbf{GBTClassifier(...)}
				\item Multilayer perceptron classifier - \textbf{MultilayerPerceptronClassifier(...)}
				\item Linear Support Vector Machine - \textbf{LinearSVC(...)}
				\item One-vs-Rest classifier - \textbf{OneVsRest(...)}
				\item Naive Bayes - \textbf{NaiveBayes(...)}
			\end{itemize}
			\item Regression
			\begin{itemize}
				\item Linear regression - \textbf{LinearRegression(...)}
				\item Generalized linear regression - \textbf{GeneralizedLinearRegression(...)}
				\item Decision tree regression - \textbf{DecisionTreeRegressor(...)}
				\item Random forest regression - \textbf{RandomForestRegressor(...)}
				\item Gradient-boosted tree regression - \textbf{GBTRegressor(...)}
				\item Survival regression - \textbf{AFTSurvivalRegression(...)}
				\item Isotonic regression - \textbf{IsotonicRegression(...)}
			\end{itemize}
			\item Linear methods
			\item Decision trees
			\item Tree ensembles
			\begin{itemize}
				\item Random forest
				\item Gradient-boosted trees (BGTs)
			\end{itemize}
			\item Clustering
			\begin{itemize}
				\item K-means - \textbf{KMeans(...)}
				\item Latent Dirichlet allocation (LDA) - \textbf{LDA(...)}
				\item Bisecting K-means - \textbf{BisectingKMeans(...)}
				\item Gaussian Mixture Model (GMM) - \textbf{GaussianMixture(...)}
			\end{itemize}
		\end{itemize}

		\paragraph{Demonstration}To demonstrate our understanding of an
		algorithm, we decided to implement a small script that uses the Decision
		tree classifier. It’s available in the repository under \emph{/q4/ }folder.

		\paragraph{}What it does is predict the revenue of individuals based on different
		characteristics, such as the age, the workclass, the education level, and many
		more parameters. Each implementation step is clearly defined in the script
		itself. The data we used comes from the UCI Machine Learning Repository \cite{16} and
		the implementation is inspired by these articles \cite{17}\cite{18}.

		\section{Predictions using Machine Learning}
			\subsection{Classifiers Choice}
				\paragraph{} In order to achieve the best predictions possible,
				we looked at 3 different machine learning algorithms. The first
				one, Support Vector Machines algorithm (SVM) was imposed. In
				order to choose the last two, we read through documentation and
				researches. In 2017, an extensive study on classification
				performance of 11 well-known classification algorithms showed
				that Stochastic Gradient Boosting Trees (GBDT), Random Forests
				(RF) and SVM are the ones achieving the best average
				classification accuracy. \cite{2} We validated those choices with
				Scikit-learn algorithm cheat-sheet. \cite{3} With multiple sources
				confirming that GBDT and RF were good candidates, we decided to
				go forward with these two classifiers algorithms.
		
			\subsection{Parameter Tuning}
				\paragraph{}Before choosing the best classification algorithm
				between SVM, RF and GBDT, we decided to try different
				combinations of parameters on each of them using the training
				dataset. By optimizing the parameters, we believed that we could
				achieve better performance and produce a better model that would
				lead to better predictions.

				\paragraph{}For each classifier, we produced a script named
				\emph{<classifierName>ParamTuning.py}. In this script, we used
				Scikit-Learn's GridSearchCV that implements fit and score methods.
				We passed multiple values for each parameter of the classifier
				to GridSearchCV, and after performing all models combinations,
				GridSearchCV returned the best parameters leading to the best
				score/model. After getting the first combination of parameters,
				we updated the parameters possible values by selecting new
				values closer to the optimal solution found previously. This
				technique is called \emph{local search}, and is used to find a good
				enough solution to a very complicated problem. In our case,
				performing all the models with all the combinations of
				parameters possible would have been way too long. Trying to find
				a local optimal is a good enough process that leaded to an increase
				in our models performance, as the following results show. We
				iterated and performed the local search as long as the model
				achieved a better score by at least 0.001.

				{\centering
					\captionof{table}{SVM Parameters Tuning} \label{tab:svm_parameters_tuning}
					\begin{tabular}{|c|c|c|c|c|}
						\hline
						\multirow{2}{*}{Iteration} & \multicolumn{3}{c}{Parameters}\vline & \multirow{2}{*}{Best Score}\\\cline{2-4}
						 & C & Gamma & Degree &\\
						\hline
						1 & 10 & 0.001 & 3 & 0.67653\\
						2 & 5 & 0.001 & 3 & 0.67653\\
						3 & 8 & 0.001 & 3 & 0.67755\\
						4 & 8 & 0.001 & 3 & 0.67755\\
						5 & 6 & 0.001 & 3 & 0.67755\\
						\hline
					\end{tabular}
				\par }

				{\centering
					\captionof{table}{RF Parameters Tuning} \label{tab:rf_parameters_tuning}
					\begin{tabular}{|c|c|c|c|c|c|}
						\hline
						\multirow{2}{*}{Iteration} & \multicolumn{4}{c}{Parameters}\vline & \multirow{2}{*}{Best Score}\\\cline{2-5}
						 & n\_estimators & max\_depth & min\_samples\_split & min\_samples\_leaf &\\
						\hline
						1 & 10 & 6 & 2 & 10 & 0.33878\\
						2 & 15 & 6 & 2 & 15 & 0.37194\\
						3 & 15 & 8 & 2 & 23 & 0.39694\\
						\hline
					\end{tabular}
				\par }

				{\centering
					\captionof{table}{GBRT Parameters Tuning} \label{tab:gbrt_parameters_tuning}
					\begin{tabular}{|c|c|c|c|c|c|c|}
						\hline
						\multirow{3}{*}{Iteration} & \multicolumn{5}{c}{Parameters}\vline & \multirow{3}{*}{Best Score}\\\cline{2-6}
						 & learning\_ & n\_ & \multirow{2}{*}{max\_depth} & min\_samples\_ & min\_samples\_ &\\
						 & rate & estimators & & split & split &\\
						\hline
						1 & 1 & 1 & 6 & 2 & 2 & 0.238\\
						2 & 0.0001 & 2 & 6 & 3 & 2 & 0.242\\
						3 & 0.0001 & 3 & 6 & 2 & 2 & 0.257\\
						4 & 0.0001 & 5 & 6 & 2 & 2 & 0.268\\
						5 & 0.0001 & 8 & 6 & 3 & 2 & 0.271\\
						6 & 0.0001 & 12 & 6 & 2 & 2 & 0.274\\
						7 & 0.0001 & 18 & 6 & 2 & 2 & 0.275\\
						8 & 0.0001 & 27 & 6 & 2 & 2 & 0.275\\
						\hline
					\end{tabular}
				\par }

				\paragraph{}Results show that parameter tuning leaded to
				increase in the models accuracies. For SVM, a gain of 0.00102
				resulted from tuning the parameters. For RF, an increase in the
				model accuracy of 0.05816 resulted from tuning the parameters.
				Finally, for GBRT, an increase of 0.037245 resulted from
				changing the parameters. Even if SVM showed the worst increase
				in accuracy from parameter tuning, it still performed better
				than RF and GBRT by achieving a score of 0.67755 compared to
				0.39694 and 0.275 for RF and GBRT. It is important to remember
				that the best parameters we obtained for each algorithms are
				local optimums and that maybe better parameters combinations
				exist.

				\paragraph{}For the predictions, we will produce a model with SVM and the
				following parameters: C = 6, gamma = 0.001 and degree = 3. This
				model should be accurate at a level of 0.6775510204081633. In
				order to validate that this accuracy is effectively of around
				0.677 independently of the input dataset, we computed the score
				of the model with a different dataset. The score obtained was
				0.6867704280155642, therefore reinforcing that the accuracy
				computed earlier is valid.

			\subsection{Model Execution and Prediction Results}
					\subsubsection{Setup}
						\paragraph{}In order to run predictions using the
						generated model with a Lambda Function on AWS, there is
						a little setup needed. The library and its dependencies
						(ex. Numpy) need to be packaged along with the function
						code in a compressed file. The created zip file can then
						be used as code entry in AWS lambda.

						\paragraph{Step 1 - Create Isolated Python Environment
						environment:}At the root of the EC2 instance used, we
						created a Python environment using the Virtualenv
						library that can be installed using \emph{pip3}. \cite{4} After
						activating the environment, we installed the Sklearn
						library. The installed libraries can be found under
						\emph{lib/python3.7/site-packages/}. The Python environment now
						contains all the libraries needed to run a Lambda
						Function that needs Sklearn.

						\paragraph{Step 2 - Adding the Lambda Function
						Code:}Inside the \emph{site-packages} folder mentioned above, we
						needed to insert our Lambda Function code under the name
						\emph{predict.py}. Inside this file, before importing Sklearn,
						we needed to add a section of code which loads libraries
						manually inside the folder. The rest of the code
						contains the logic to download the model from S3,
						run the predictions on the input dataset and
						return the results.
				
						\paragraph{Step 3 - Uploading Python Environment to
						S3:}To enable AWS Lambda to run the code,
						it needs to be configured to upload a zip file from S3
						as the code entry type.

						\begin{center}
							\begin{minipage}{\textwidth}
								\captionof{figure}{} \label{fig:function_code}
								\includegraphics[width=\linewidth]{lambda_function_code.png}
							\end{minipage}
						\end{center}

						\paragraph{} The zip file to upload to S3 needs to
						contain all the objects inside the \emph{site-packages} folder
						mentioned earlier. Once the zip file was created, we
						uploaded it to S3 using a script named
						\emph{uploadFileToBucket.py}.

						\paragraph{Step 4 - Call the Function:}The function can
						now be called and will use Sklearn without trouble. The
						script to call the function is named
						\emph{invokeLambdaFunction.py} and the argument needed is
						\emph{ds1Test-1.csv}.

					\subsubsection{Results}
						\paragraph{}The results produced by the model with the
						\emph{ds1Test} dataset are printed in the csv file named
						\emph{predictions.csv}.

\section{Future Directions}
	\paragraph{} This work was produced in a limited time and hardware
	resources context. To achieve better results, it would have been
	interesting to consider more than 3 classifiers algorithms and use a
	better parameter tuning algorithm where more possibilities across an
	higher range would have been tested.

\section{Conclusion}
	\paragraph{} In this work, we learned how to install and use Spark to
	perform heavy calculations efficiently on a EC2 instance. We also learned
	how to use AWS S3 buckets to easily store, use and share data from different AWS
	services. The tools acquired during the work enabled us to produce
	predictions using a machine learning model that can be ran using Lambda
	functions on AWS.

\begin{thebibliography} {}
	\bibitem{1} Apache Spark. RDD Programming Guide. [Online]. Available:\url{https://spark.apache.org/docs/latest/rdd-programming-guide.html}
	\bibitem{2} C. Zhang and al., “An up-to-date comparison of state-of-the-art classification algorithms,” Expert Systems With Applications, vol. 82, p. 125-150, Apr. 2017. [Online]. Available:\url{http://th-www.if.uj.edu.pl/~erichter/forMichal/papers-ML/1-s2.0-S0957417417302397-main.pdf}
	\bibitem{3} scikit-learn developers. (2018) Choosing the right estimator. [Online]. Available:\url{https://scikit-learn.org/stable/tutorial/machine_learning_map/index.html}
	\bibitem{4} DreamHost. (2019) Installing and using virtualenv with Python 3. [Online]. Available:\url{https://help.dreamhost.com/hc/en-us/articles/115000695551-Installing-and-using-virtualenv-with-Python-3}
	\bibitem{5} Zaharia, Matei, et al. "Spark: Cluster computing with working sets." HotCloud 10. 2010 [Online]. Available:\url{http://static.usenix.org/events/hotcloud10/tech/full_papers/Zaharia.pdf}
	\bibitem{6} Deanand, Ghemawa. “MapReduce:SimpliedDataProcessingonLargeClusters.”. OSDI 04.2004. [Online]. Available:\url{https://static.googleusercontent.com/media/research.google.com/en//archive/mapreduce-osdi04.pdf}
	\bibitem{7} Dzone. (2015) What Is RDD in Spark and Why Do We Need It? [Online]. Available:\url{https://dzone.com/articles/what-is-rdd-in-spark-and-why-do-we-need-it}
	\bibitem{8} Hortonworks. (2018) Hortwonworks Data Platform. [Online]. Available:\url{https://hortonworks.com/products/data-platforms/hdp/}
	\bibitem{9} IBM. (2018) IBM analytics for Spark. [Online]. Available:\url{https://www.ibm.com/ca-en/marketplace/spark-as-a-service?mhq=spark&mhsrc=ibmsearch_p}
	\bibitem{10} Pivotal. (2018) Pivotal Greenplum. [Online]. Available:\url{https://pivotal.io/pivotal-greenplum}
	\bibitem{11} DataStax. (2018) DataStax Entreprise analytics. [Online]. Available:\url{https://www.datastax.com/products/datastax-enterprise-analytics}
	\bibitem{12} Databricks. (2018) DataBricks Unified Analytics Platform. [Online]. Available:\url{https://databricks.com/product/unified-analytics-platform}
	\bibitem{13} MapR. (2018) The MapR Data Platform. [Online]. Available:\url{https://mapr.com/products/}
	\bibitem{14} Cloudera. (2018) Apache Spark. [Online]. Available:\url{https://www.cloudera.com/products/open-source/apache-hadoop/apache-spark.html}
	\bibitem{15} Databricks. Databricks Launches “Certified Apache Spark Distribution” Program. [Online]. Available:\url{https://databricks.com/blog/2014/06/26/databricks-launches-certified-spark-distribution-program.html}
	\bibitem{16} UCI Machine Learning Repository. Adults Dataset. [Online]. Available:\url{http://archive.ics.uci.edu/ml/datasets/Adult}
	\bibitem{17} Scalac. Introduction to Machine Learning with Spark and MLLib. [Online]. Available:\url{https://blog.scalac.io/scala-spark-ml.html}
	\bibitem{18} Towards Data Science. Build an end-to-end Machine Learning Model with MLLib in pySpark. [Online]. Available:\url{https://towardsdatascience.com/build-an-end-to-end-machine-learning-model-with-mllib-in-pyspark-4917bdf289c5}
\end{thebibliography}

\end{document}