import csv
import subprocess
import math
import os 
import numpy as np
from sklearn import svm
from sklearn.model_selection import GridSearchCV

# Code inspired from https://chrisalbon.com/machine_learning/model_evaluation/cross_validation_parameter_tuning_grid_search/?fbclid=IwAR0J1SSe4exEsP79ciRqGYYSLcjhDiAAPy02cUGp5O4nhG4FmuaEG3qlY0c

if(os.path.isfile('./ds1Train.csv') == False):
    subprocess.call('python3 fetchDataset.py')

resultsFile = open("svc_param_tuning.csv", 'w+') 
resultsFile.write("C_best_value, gamma_best_value, degree_best_value, best_score\n")

data = []
classes = []
with open("ds1Train.csv") as trainingFile:
    reader = csv.reader(trainingFile)
    for row in reader:
        classes.append(row.pop())
        data.append(row)

parametersSets = [{'C': [1,10,100, 1000], 'kernel':['poly'], 'gamma': [0.1, 0.01, 0.001, 0.0001], 'degree': [3, 4, 5]}]
model = GridSearchCV(estimator=svm.SVC(), param_grid=parametersSets, n_jobs=-1)
model.fit(data, classes)
currentBestScore = model.best_score_
C_best_value = model.best_estimator_.C
gamma_best_value = model.best_estimator_.gamma
degree_best_value = model.best_estimator_.degree
print(str(C_best_value) + ", " + str(gamma_best_value) + ", " + str(degree_best_value) + ", " + str(currentBestScore))
resultsFile.write(str(C_best_value) + ", " + str(gamma_best_value) + ", " + str(degree_best_value) + ", " + str(currentBestScore) + "\n")
C_step = C_best_value / 2.0
gamma_step = gamma_best_value / 2.0
degree_step = degree_best_value / 2.0

scoreIsImproving = True
threshold = 0.001
nSamples = 3
C_absolute_min_value = 1
gamma_absolute_min_value = 0.0000001
degree_absolute_min_value = 3
counter = 0

while (scoreIsImproving or counter <  2) :
    # C samples
    C_max_value = math.ceil(C_best_value + C_step)
    C_min_value = C_best_value - C_step
    if C_min_value < C_absolute_min_value:
        C_min_value = C_absolute_min_value
        C_sample = np.linspace(C_min_value, C_max_value, 2)   
    else: 
        C_sample = np.linspace(C_min_value, C_max_value, nSamples)
    
    # gamma samples
    gamma_max_value = gamma_best_value + gamma_step
    gamma_min_value = gamma_best_value - gamma_step
    if gamma_min_value < gamma_absolute_min_value:
        gamma_min_value = gamma_absolute_min_value
        gamma_sample = np.linspace(gamma_min_value, gamma_max_value, 2)
    else:
        gamma_sample = np.linspace(gamma_min_value, gamma_max_value, nSamples)
    
    # degree samples
    degree_max_value = math.ceil(degree_best_value + degree_step)
    degree_min_value = degree_best_value - degree_step
    if degree_min_value < degree_absolute_min_value:
        degree_min_value = degree_absolute_min_value
        degree_sample = np.linspace(degree_min_value, degree_max_value, 2)   
    else: 
        degree_sample = np.linspace(degree_min_value, degree_max_value, nSamples)
    

    parametersSets = [{'C': C_sample, 'kernel':['poly'], 'gamma': gamma_sample, 'degree': degree_sample}]
    model = GridSearchCV(estimator=svm.SVC(), param_grid=parametersSets, n_jobs=-1)
    model.fit(data, classes)
    score = model.best_score_
    computedSameParameters = (model.best_estimator_.C == C_best_value) and (model.best_estimator_.gamma == gamma_best_value) and (model.best_estimator_.degree == degree_best_value)
    scoreIsImproving = ((score - currentBestScore) > threshold) or (computedSameParameters == True) 
    if(score > currentBestScore):
        currentBestScore = score

    # Step tunning
    C_step = model.best_estimator_.C / 2.0
    if(model.best_estimator_.C == C_best_value):
        C_step = C_step / 2.0
    gamma_step = model.best_estimator_.gamma / 2.0
    if(model.best_estimator_.gamma == gamma_best_value):
        gamma_step = gamma_step / 2.0
    gamma_step = model.best_estimator_.gamma / 2.0
    if(model.best_estimator_.degree == degree_best_value):
        degree_step = degree_step / 2.0
    
    C_best_value = model.best_estimator_.C
    gamma_best_value = model.best_estimator_.gamma
    degree_best_value = model.best_estimator_.degree
    resultsFile.write(str(C_best_value) + ", " + str(gamma_best_value) + ", " + str(degree_best_value) + ", " + str(score) + "\n")
    print(str(C_best_value) + ", " + str(gamma_best_value) + ", " + str(degree_best_value) + ", " + str(score))
    counter = counter + 1
    computedSameParameters = False
    

resultsFile.close()
