from pyspark import SparkContext, SparkConf
from operator import add

# https://data-flair.training/blogs/spark-rdd-operations-transformations-actions/
# https://spark.apache.org/docs/latest/rdd-programming-guide.html


def mapTransformation(data):
    # Applies a function on the data
    # Capitalize all letters of every line
    results = data.map(lambda line: line.upper()).collect()
    print("map() results:")
    for result in results:
        print(" " + str(result))
    print(" ")


def flatMapTransformation(data):
    # Applies a function on the data
    # Split all the lines into words
    results = data.flatMap(lambda line: line.split(" ")).collect()
    print("flatMap() results:")
    for result in results:
        print(" " + str(result))
    print(" ")


def filterTransformation(data):
    # Returns elements meeting the predicate
    # Return the lines that contains exactly 5 words
    results = data.filter(lambda line: len(line.split(" ")) == 5).collect()
    print("filter() results:")
    for result in results:
        print(" " + str(result))
    print(" ")


def mapPartitionsTransformation(data):
    def upperFunction(iterator):
        lines = list(iterator)
        lines = list(map(str.upper, lines))
        yield str(lines)

    # Applies a function on the data in each partition
    # Capitalize all letters of every line
    results = data.mapPartitions(upperFunction).collect()
    print("mapPartitions() results:")
    for result in results:
        print(" " + str(result))
    print(" ")


def mapPartitionsWithIndexTransformation(data):
    def indexFunction(index, iterator):
        yield 'index: ' + str(index) + ' line: ' + str(list(iterator))

    # Applies a function on the data in each partition
    # Returns all lines from the different partitions (by partition index)
    results = data.mapPartitionsWithIndex(indexFunction).collect()
    print("mapPartitionsWithIndex() results:")
    for result in results:
        print(" " + str(result))
    print(" ")


def unionTransformation(data):
    # Combines to RDD
    # Combines the upper case lines of 2 RDD
    rdd1 = data.map(lambda line: line.upper())
    data2 = sc.textFile("data2.txt")
    rdd2 = data2.map(lambda line: line.upper())
    results = rdd1.union(rdd2).collect()
    print("unionTransformation() results:")
    for result in results:
        print(" " + str(result))
    print(" ")


def intersectionTransformation(data):
    # Intersection of RDDs, results show the common elements only
    rdd1 = data.map(lambda line: line.upper())
    data2 = sc.textFile("data2.txt")
    rdd2 = data2.map(lambda line: line.upper())
    results = rdd1.intersection(rdd2).collect()
    print("intersectionTransformation() results:")
    for result in results:
        print(" " + str(result))
    print(" ")


def distinctTransformation(data):
    # Removes duplicate in a dataset
    results = data.distinct().collect()
    print("distinctTransformation() results:")
    for result in results:
        print(" " + str(result))
    print(" ")


def groupByKeyTransformation(data):
    # Groups all element with the same key in a single object
    # Group all words together
    results = data.flatMap(lambda line: line.split(" ")).map(
        lambda word: (word, 1)).groupByKey().collect()
    print("groupByKeyTransformation() results:")
    for result in results:
        print(" ('" + str(result[0]) + "', " + str(list(result[1])
                                                   ) + ", count: " + str(len(list(result[1]))) + ")")
    print(" ")


def reduceByKeyTransformation(data):
    # Group similar key-value pairs by key on the same machine
    results = data.flatMap(lambda line: line.split(" ")).map(
        lambda word: (word, 1)).reduceByKey(add).collect()
    print("reduceByKeyTransformation() results:")
    for result in results:
        print(" " + str(result))
    print(" ")


def sortByKeyTransformation(data):
    # Sort a dataset of key-values by the key
    results = data.flatMap(lambda line: line.split(" ")).map(
        lambda word: (word, 1)).reduceByKey(add).sortByKey().collect()
    print("sortByKeyTransformation() results:")
    for result in results:
        print(" " + str(result))
    print(" ")


def joinTransformation(data):
    # Joins 2 Rdd based on the key of the elements. Only common elements will be selected.
    rdd1 = data.flatMap(lambda line: line.split(" ")).map(
        lambda word: (word, 1)).reduceByKey(add).sortByKey()
    data2 = sc.textFile("data2.txt")
    rdd2 = data2.flatMap(lambda line: line.split(" ")).map(
        lambda word: (word, 1)).reduceByKey(add).sortByKey()
    results = rdd1.join(rdd2).collect()
    print("joinTransformation() results:")
    for result in results:
        print(" " + str(result))
    print(" ")


def coalesceTransformation(data):
    # Decrease number of partitions used.
    rdd1 = data.map(lambda line: line.upper())
    results = rdd1.coalesce(1).collect()
    print("coalesceTransformation() results:")
    for result in results:
        print(" " + str(result))
    print(" ")


def sampleTransformation(data):
    # Take a sample of the produced rdd
    rdd1 = data.map(lambda line: line)
    results = rdd1.sample(False, 0.2).collect()
    print("sampleTransformation() results:")
    for result in results:
        print(" " + str(result))
    print(" ")


def aggregateByKeyTransformation(data):
    # Aggregates in the partitions before aggregating outside
    results = data.flatMap(lambda line: line.split(" ")).map(
        lambda word: (word, 1)).aggregateByKey(0, add, add).collect()
    print("aggregateByKeyTransformation() results:")
    for result in results:
        print(" " + str(result))
    print(" ")


def cogroupTransformation(data):
    # Combines key-values element of different datasets but conserves the values associated in a tuple(Iterable<V1>, Iterable<V2>)
    rdd1 = data.flatMap(lambda line: line.split(" ")).map(
        lambda word: (word, 1)).reduceByKey(add)
    data2 = sc.textFile("data2.txt")
    rdd2 = data2.flatMap(lambda line: line.split(" ")).map(
        lambda word: (word, 1)).reduceByKey(add)
    results = rdd1.cogroup(rdd2).collect()
    print("cogroupTransformation() results:")
    for result in results:
        print(" " + str(result))
    print(" ")


def reduceAction(data):
    # Aggregates elements using a function passed.
    # Counts the number of words in the document
    results = data.flatMap(lambda line: line.split(" ")).map(
        lambda word: 1).reduce(add)
    print("reduceAction() results:")
    print(" " + str(results))
    print(" ")


def collectAction(data):
    # Creates an array with the elements of the rdd
    # Creates an array with the lines
    results = data.map(lambda line: line).collect()
    print("collectAction() results:")
    for result in results:
        print(" " + str(result))
    print(" ")


def countAction(data):
    # Gives the number of elements
    # Gives the number of words in the document
    results = data.flatMap(lambda line: line.split(" ")).count()
    print("countAction() results:")
    print(" " + str(results))
    print(" ")


def firstAction(data):
    # Gives the first element
    # Gives the first word in the document
    results = data.flatMap(lambda line: line.split(" ")).first()
    print("firstAction() results:")
    print(" " + str(results))
    print(" ")


def takeAction(data):
    # Gives an array with the x first elements of the rdd
    # Gives the first 2 words of the first line
    results = data.flatMap(lambda line: line.split(" ")).take(2)
    print("takeAction() results:")
    print(" " + str(results))
    print(" ")


def takeSampleAction(data):
    # Gives an array with x elements randomly selected in the dataset
    # Gives an array with 4 random words from the dataset
    results = data.flatMap(lambda line: line.split(" ")).takeSample(False, 4)
    print("takeSampleAction() results:")
    print(" " + str(results))
    print(" ")


def takeOrderedAction(data):
    # Gives an array of x elements ordered by default or with a specific orderer
    # Gives the first 3 words of the document by alphabetic order, starting with capitalized words
    results = data.flatMap(lambda line: line.split(" ")).takeOrdered(3)
    print("takeOrderedAction() results:")
    print(" " + str(results))
    print(" ")


def countByKeyAction(data):
    # Returns keys with associated number of occurences
    # Returns all works as keys with their number of occurences
    results = data.flatMap(lambda line: line.split(" ")).map(
        lambda word: (word, 1)).countByKey()
    print("countByKeyAction() results:")
    for result in results:
        print(" " + str(result) + ", " + str(results.get(result)))
    print(" ")


# create Spark context with Spark configuration
conf = SparkConf().setAppName("Spark Commands")
sc = SparkContext(conf=conf)
data = sc.textFile("data.txt")

print(" ")
print(" ")
# Transformations
mapTransformation(data)
flatMapTransformation(data)
filterTransformation(data)
mapPartitionsTransformation(data)
mapPartitionsWithIndexTransformation(data)
unionTransformation(data)
intersectionTransformation(data)
distinctTransformation(data)
groupByKeyTransformation(data)
reduceByKeyTransformation(data)
sortByKeyTransformation(data)
joinTransformation(data)
coalesceTransformation(data)
sampleTransformation(data)
aggregateByKeyTransformation(data)
cogroupTransformation(data)
# Other transformations include cartesian, pipe, repartition and repartitionAndSortWithinPartitions

# Actions
reduceAction(data)
collectAction(data)
countAction(data)
firstAction(data)
takeAction(data)
takeSampleAction(data)
takeOrderedAction(data)
countByKeyAction(data)
# Other actions include saveAsTextFile and foreach.
