import boto3
import sys
import json
import csv 

client = boto3.client('lambda')
filePath = sys.argv[1]

dataset = {}
dataset['values'] = []
with open(filePath) as dataFile:
    reader = csv.reader(dataFile)
    for row in reader:
        dataset['values'].append(row)
dataFile.close()
response = client.invoke(
    FunctionName='predict',
    LogType='None',
    InvocationType='RequestResponse',
    Payload=json.dumps(dataset)
)
response = json.loads(response['Payload'].read().decode())
print(response)
with open('predictions.csv', 'w+') as predictionsFile:
    writer = csv.writer(predictionsFile)
    for value in response['body']:
        writer.writerow([value])
predictionsFile.close()
