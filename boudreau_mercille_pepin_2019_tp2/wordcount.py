#!/usr/bin/python3
from pyspark import SparkContext, SparkConf

if __name__ == "__main__":

  # create Spark context with Spark configuration
  conf = SparkConf().setAppName("Word Count")
  sc = SparkContext(conf=conf)

text_file = sc.textFile("219-0.txt")
counts = text_file.flatMap(lambda line: line.split(" ")) \
             .map(lambda word: (word, 1)) \
             .reduceByKey(lambda a, b: a + b)
counts.saveAsTextFile("file:///home/ubuntu/log8415-tp2/boudreau_mercille_pepin_2019_tp2/wordcount_results")
