import boto3
import botocore

BUCKET_NAME = 'log8430tp2' 
KEY = 'ds1Test-1.csv'
s3 = boto3.resource('s3')

try:
    s3.Bucket(BUCKET_NAME).download_file(KEY, 'ds1Test-1.csv')
except botocore.exceptions.ClientError as e:
    if e.response['Error']['Code'] == "404":
        print("The object does not exist.")
    else:
        raise

KEY='ds1Val.csv'

try:
    s3.Bucket(BUCKET_NAME).download_file(KEY, 'ds1Val.csv')
except botocore.exceptions.ClientError as e:
    if e.response['Error']['Code'] == "404":
        print("The object does not exist.")
    else:
        raise
